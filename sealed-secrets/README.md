Need to add a key prior to using already generated secrets

From the machine with the **master.key** backup:

```
kubectl apply -f master.key
```

The backup of the keys is done the following way:

```
kubectl get secret -n sealed-secrets -l sealedsecrets.bitnami.com/sealed-secrets-key -o yaml >master.key
```

The public key backup can be done, assuming **kubeseal** client is installed and has access to the kubernetes cluster, this way:

```
kubeseal --controller-namespace sealed-secrets --fetch-cert >mycert.pem
```

